SETTINGS = {
    # Experiment structure
    'NUM_CHAINS_PER_PARTICIPANT': 2,
    'TIME_ESTIMATE_PER_TRIAL': 20,  # seconds
    'TARGET_PARTICIPANTS': 150,
    'INITIAL_RECRUITMENT_SIZE': 11,
    'NUM_SWIPES': 3,  # how often each dimension is repeated
    
    # Slider settings
    'REVERSE_SCALE': False,
    'NUMBER_OF_SLIDER_TICKS': 25,
    'SNAP_SLIDER': False,
    'AUTOPLAY': True,
    'MIN_DURATION': 10,
    'DEBUG': False,

    # Experimental conditions
    'TARGETS': ['happy', 'sad', 'angry'],
    'RANGES': [
        # DURATION
        # 1. Duration, percent
        [0.8, 1.2],

        # INTENSITY
        # 2. Tremolo rate, st
        [0.01, 5],

        # 3. Tremolo depth, dB
        [0.01, 10],

        # PITCH
        # 4. Shift, semitones
        [-3, 3],

        # 5. Range, percent
        [0.2, 1.8],

        # 6. Increase/Decrease, semitones
        [-3, 3],

        # 7. Jitter, custom unit
        [0, 10]
    ],
    'INITIAL_VALUES': [
        1,  # 1. Duration, percent
        0.1,  # 2. Tremolo rate, st
        0.05,  # 3. Tremolo depth, dB
        0,  # 4. Shift, semitones
        1,  # 5. Range, percent
        0,  # 6. Increase/Decrease, semitones
        0  # 7. Jitter, custom unit
    ],
    'DIMENSION_NAMES': [
        'duration', 'tremolo_rate', 'tremolo_depth', 'pitch_shift', 'pitch_range', 'pitch_change', 'jitter'
    ],


    'SENTENCE_RECORDINGS': ['Harvard_L35_S01_0.wav', 'Harvard_L35_S02_0.wav', 'Harvard_L35_S03_0.wav']
}

SETTINGS['DIMENSIONS'] = len(SETTINGS['RANGES'])

SETTINGS['NUM_TRAILS_PER_CHAIN'] = SETTINGS['NUM_SWIPES'] * SETTINGS['DIMENSIONS']
