from flask import Flask
from gibbsvisualizer import GibbsVisualizer

app = Flask(__name__)


def index():
    return "Fistro diodenarl de abajorl"


dimension_names = ['duration', 'tremolo_rate', 'tremolo_depth', 'pitch_shift', 'pitch_range', 'pitch_change', 'jitter']


def extract_data(info, network, dimension_names):
    import pandas as pd
    import json
    nrows, ncols = info.shape

    data = pd.DataFrame()

    # Dictionary to keep track of all iterations within a network
    iteration_count = {}

    # Filter out training sessions
    allowed_networks = network['id'][network['role'] == 'experiment'].values

    for r in range(nrows):
        # Only interested in trials that are a trial and are real experiment trials (not practice trials)
        network_id = info.loc[r, 'network_id']

        # Read the contents
        contents = json.loads(info.loc[r, "contents"])
        details = json.loads(info.loc[r, "details"])

        is_valid_iteration = 'vector' in contents  # Make sure you have the vector with the dimensions
        is_valid_iteration = is_valid_iteration and 'answer' in details  # Check if the trial has an answer
        is_valid_iteration = is_valid_iteration and network_id in allowed_networks  # Only allow experiment networks
        is_valid_iteration = is_valid_iteration and info.loc[r, 'failed'] == 'f'  # make sure the trial was not failed

        if is_valid_iteration:
            # Make sure the vector matches all dimensions
            if 'vector' in contents and len(contents['vector']) != len(dimension_names):
                raise ValueError('Each dimension must have exactly one value in the vector')

            # The vector is the initial stimulus presented to the participant,
            # but we want to have the stimulus the participant picked
            vector = contents['vector']

            answer = details['answer']
            active_index = contents['active_index']
            random_int = vector[active_index]
            vector[active_index] = answer

            # Put them together
            row = dict(zip(dimension_names, vector))
            row['active_index'] = active_index
            row['active_dimension'] = dimension_names[active_index]
            row['random_int'] = random_int

            # Copy information from the current row
            column_mapping = [
                ('network_id', 'network_id'),
                ('type', 'type'),
                ('id', 'id'),
                ('participant', 'property1'),
            ]
            for new_colname, old_colname in column_mapping:
                row[new_colname] = info.loc[r, old_colname]

            # Compute the iteration within the network
            iteration = 1
            if network_id in iteration_count:
                iteration = iteration_count[network_id] + 1
            iteration_count[network_id] = iteration
            row['iteration'] = iteration

            network_details = json.loads(network.loc[network.id == network_id, "details"].values[0])
            file = network_details['definition']['file']
            row['sentence'] = file[14]

            if iteration == 21:
                from temp.synthesis import make_stimuli
                chain_definition = {}
                chain_definition['file'] = file
                output_path = 'examples/batch1_%s_%s_%s.wav' % (network_details['definition']['target'], row['sentence'], row['participant'])
                make_stimuli(vector, output_path, chain_definition)

            row['emotion'] = info.loc[r, "type"].split("_")[0]

            data = data.append(row, ignore_index=True)


    data.to_csv('batch1.csv', index=False)


def custom_synthesis():
    pass


init_argument = {
    # You can overwrite existing behaviour
    # 'index': index,
    'app_name': 'emo',
    'dimension_names': dimension_names,
    'extract_data': extract_data,
}

GibbsVisualizer.register(app, init_argument=init_argument)

if __name__ == '__main__':
    app.run()
