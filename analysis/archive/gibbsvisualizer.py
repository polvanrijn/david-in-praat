from flask_classful import FlaskView, route
from flask import render_template, request
import os
from zipfile import ZipFile
import shutil
import pandas as pd


class GibbsVisualizer(FlaskView):
    route_base = '/'
    app_name = None
    dimension_names = None
    download_data = True
    required_parameters = ['app_name', 'dimension_names']

    def __init__(self, arguments):
        if not all([param in arguments for param in self.required_parameters]):
            raise ValueError('Not all required parameters were passed: %s' % self.required_parameters)

        for key, value in arguments.items():
            self.__setattr__(key, value)



    def index(self):
        if not os.path.exists('data.csv'):
            # TODO try to download the latest version
            self.unzip_and_extract('data/batch1.zip')

        return render_template('raw-data.html')

    def extract_data(self, info, network, dimension_names):
        raise NotImplementedError


    @route('/unzip_and_extract/', methods=['GET'])  # <--- Adding route
    def unzip_and_extract_api(self):
        zip_path = request.args.get('zip_path')
        if zip_path is None:
            raise ValueError('`zip_path` is not specified')
        self.unzip_and_extract(zip_path)
        return 'Finished'

    def unzip_and_extract(self, zip_path):
        if not os.path.exists(zip_path):
            raise ValueError('The path %s does not exist' % zip_path)

        # Create a ZipFile Object and load sample.zip in it
        with ZipFile(zip_path, 'r') as zipObj:
            # Extract all the contents of zip file in current directory
            zipObj.extractall('temp')

        # extract experiment
        with ZipFile('temp/emo-code.zip', 'r') as zipObj:
            # Extract all the contents of zip file in current directory
            zipObj.extractall('temp')

        shutil.move('temp/stimuli', 'stimuli')
        os.rename('temp/settings.py', 'settings.py')

        info = pd.read_csv('temp/data/info.csv').sort_values(by='id')
        network = pd.read_csv('temp/data/network.csv').sort_values(by='id')
        self.extract_data(info, network, self.dimension_names)
        shutil.rmtree('temp', ignore_errors=True)
        shutil.rmtree('stimuli', ignore_errors=True)
        os.remove('settings.py')