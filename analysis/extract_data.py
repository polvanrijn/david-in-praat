import os
from zipfile import ZipFile
import shutil
import pandas as pd
import json
from copy import copy
from synthesis import make_stimuli


def extract_data(info, network, filename):
    nrows, ncols = info.shape

    data = pd.DataFrame()

    # Dictionary to keep track of all iterations within a network
    iteration_count = {}

    # Filter out training sessions
    allowed_networks = network['id'][(network['role'] == 'experiment') & (network['full'] == 't')].values

    for r in range(nrows):
        # Only interested in trials that are a trial and are real experiment trials (not practice trials)
        network_id = info.loc[r, 'network_id']

        # Read the contents
        contents = json.loads(info.loc[r, "contents"])
        details = json.loads(info.loc[r, "details"])

        is_valid_iteration = 'vector' in contents  # Make sure you have the vector with the dimensions
        is_valid_iteration = is_valid_iteration and 'answer' in details  # Check if the trial has an answer
        is_valid_iteration = is_valid_iteration and network_id in allowed_networks  # Only allow experiment networks
        is_valid_iteration = is_valid_iteration and info.loc[r, 'failed'] == 'f'  # make sure the trial was not failed

        if is_valid_iteration:
            # Make sure the vector matches all dimensions
            if 'vector' in contents and len(contents['vector']) != len(dimension_names):
                raise ValueError('Each dimension must have exactly one value in the vector')

            # The vector is the initial stimulus presented to the participant,
            # but we want to have the stimulus the participant picked
            vector = contents['vector']
            initial_vector = copy(vector)
            #initial_vector = vector.copy()

            answer = details['answer']
            active_index = contents['active_index']
            random_int = vector[active_index]
            vector[active_index] = answer

            # Put them together
            row = dict(zip(dimension_names, vector))
            row['active_index'] = active_index
            row['active_dimension'] = dimension_names[active_index]
            row['random_int'] = random_int

            # Copy information from the current row
            column_mapping = [
                ('network_id', 'network_id'),
                ('type', 'type'),
                ('id', 'id'),
                ('participant', 'property1'),
            ]
            for new_colname, old_colname in column_mapping:
                row[new_colname] = info.loc[r, old_colname]

            # Compute the iteration within the network
            iteration = 1
            if network_id in iteration_count:
                iteration = iteration_count[network_id] + 1
            iteration_count[network_id] = iteration
            row['iteration'] = iteration

            network_details = json.loads(network.loc[network.id == network_id, "details"].values[0])
            file = network_details['definition']['file']
            row['sentence'] = file[14]

            row['id'] += pad_id
            row['network_id'] += pad_network_id
            row['participant'] += pad_participant
            row['emotion'] = info.loc[r, "type"].split("_")[0]

            data = data.append(row, ignore_index=True)

            if do_synthesis:
                # Do synthesis
                emotion = row['emotion'][:3].upper()
                chain_definition = {}
                chain_definition['file'] = file
                output_path = 'examples/%s_%s_%s_%s.wav'

                if iteration == 1:
                    make_stimuli(
                        initial_vector,
                        output_path % (emotion, "{:03d}".format(row['participant']), "00", row['sentence']),
                        chain_definition
                    )
                    print(output_path % (emotion, "{:03d}".format(row['participant']), "00", row['sentence']))

                # Make the stimuli
                make_stimuli(
                    vector,
                    output_path % (
                    emotion, "{:03d}".format(row['participant']), "{:02d}".format(iteration), row['sentence']),
                    chain_definition
                )
                print(output_path % (
                    emotion, "{:03d}".format(row['participant']), "{:02d}".format(iteration), row['sentence']
                ))

    data.to_csv(filename, index=False)


def unzip_and_extract(zip_path, filename):
    if not os.path.exists(zip_path):
        raise ValueError('The path %s does not exist' % zip_path)

    # Create a ZipFile Object and load sample.zip in it
    with ZipFile(zip_path, 'r') as zipObj:
        # Extract all the contents of zip file in current directory
        zipObj.extractall('temp')

    # extract experiment
    with ZipFile('temp/emo-code.zip', 'r') as zipObj:
        # Extract all the contents of zip file in current directory
        zipObj.extractall('temp')

    shutil.move('temp/stimuli', 'stimuli')
    os.rename('temp/settings.py', 'settings.py')

    info = pd.read_csv('temp/data/info.csv').sort_values(by='id')
    network = pd.read_csv('temp/data/network.csv').sort_values(by='id')
    extract_data(info, network, filename)
    shutil.rmtree('temp', ignore_errors=True)
    if remove_stim_folder:
        shutil.rmtree('stimuli', ignore_errors=True)
        os.remove('settings.py')


pad_id = 0
pad_network_id = 0
pad_participant = 0
do_synthesis = False
remove_stim_folder = True
dimension_names = ['duration', 'tremolo_rate', 'tremolo_depth', 'pitch_shift', 'pitch_range', 'pitch_change', 'jitter']
unzip_and_extract('data/exp1-within-emo-data.zip', 'part1.csv')
part1_df = pd.read_csv('part1.csv')
pad_id = int(part1_df.id.max()) + 1
pad_network_id = int(part1_df.network_id.max()) + 1
pad_participant = int(part1_df.participant.max()) + 1

remove_stim_folder = False
unzip_and_extract('data/exp2-within-emo-data.zip', 'part2.csv')
part2_df = pd.read_csv('part2.csv')

# combine into one df
df = part1_df.append(part2_df, ignore_index=True)
os.remove('part1.csv')
os.remove('part2.csv')
df.to_csv('data.csv', index=False)

# columns = copy(dimension_names)
# columns.append('emotion')
# columns.append('iteration')
# df = df[columns]
# # Compute average for last 3 iterations
# last_3_iterations = df[
#     [i in range(19,22) for i in df['iteration']]
#     ].groupby(
#     ['iteration', 'emotion']
# )[dimension_names].agg(['mean'])
#
# participant = 'avg'
# chain_definition = {}
# for r in range(last_3_iterations.shape[0]):
#     vector = last_3_iterations[dimension_names].iloc[r, :].values
#     emotion = last_3_iterations.index[r][1][:3].upper()
#     iteration = int(last_3_iterations.index[r][0])
#
#     output_path = 'examples/%s_%s_%s_%s.wav'
#     for s in range(1, 4):
#         chain_definition['file'] = 'Harvard_L35_S0%d_0.wav' % s
#         # Make the stimuli
#         make_stimuli(
#             vector,
#             output_path % (
#                 emotion, participant, "{:02d}".format(iteration), s),
#             chain_definition
#         )
#         print(output_path % (emotion, participant, "{:02d}".format(iteration), s))

# For plotting
# last_iteration = df_norm[df_norm['iteration'] == 21].groupby(['iteration', 'emotion'])[dimension_names].agg(['mean'])
# last_iteration['emotion'] =  [i[1] for i in last_iteration.index]
# last_iteration_long = pd.melt(last_iteration,id_vars=['emotion'],var_name='x', value_name='y')
# import seaborn as sns
# import matplotlib.pyplot as plt
# ax = sns.lineplot(x="x", y="y", hue='emotion', data=last_iteration_long, palette=['red', 'green', 'blue'])
# plt.ylim([0,1])
# plt.show()
