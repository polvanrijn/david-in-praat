# MOODS1

#### Using a Gibbs slider to elicit prototypes of emotional prosody



## Playground

With a Flask app you can play with all acoustic dimensions. This will help to get a feel for the single dimensions and how they interact. The app is in the folder `playground`.

You need to make sure you have all dependencies in `requirements.txt` installed. To run the app, go to the directory and run:

```
flask run
```

Then open http://127.0.0.1:5000/ with Chrome

## Experiment

In the experiment, we'll use the Gibbs slider paradigm to parametrically modify speech recordings to make them sound emotional.

### Scenarios

We will use the emotions: happy, sad and angry. Each participant will be asked for only one emotion, e.g. participant 1 gets happiness, participant 2 gets sadness etc. Each emotion is introduced with a context. The contexts have been used in studies by [Laukka et al (2016)](https://psycnet.apa.org/fulltext/2016-40098-001.html) and [Cowen et al (2018)](https://static-content.springer.com/esm/art%3A10.1038%2Fs41562-019-0533-6/MediaObjects/41562_2019_533_MOESM1_ESM.pdf):

- **Anger**: Think of a situation where you experienced a *demeaning offense against you and yours.* For example, somebody behaves rudely toward you and hinders you from achieving a valued goal. The situation is unexpected and unpleasant, but you have the power to retaliate.
- **Happiness**: Think of a situation where you *made reasonable progress toward the realization of a goal.* For example, you have succeeded in achieving a valued goal. Your success may be due to your own actions, or somebody else’s, but the situation is pleasant and you feel active and in control.
- **Sadness**: Think of a situation where you experienced *an irrevocable loss.* For example, you lose someone or something very valuable to you, and you have no way of getting back that what you want.

### Acoustic dimensions

*Duration:*

1. D1: Duration

*Intensity*

2. ~~I1: Gain (in dB)~~ excluded because it can be modified by the user
3. I2: Tremelo rate (pulses per second)
4. I3: Tremelo depth (in dB)



*Pitch*

5. P1: Pitch shift
6. P2: Pitch range
7. P3: Pitch slope
8. P4: Jitter (0 to 100)
9. ~~P6: Vibrato rate~~
10. ~~P7: Vibrato depth~~



*Frequency filtering*

1. Cutoff value
2. Gain value



~~*Formants*~~ excluded because it produces many artefacts

1. ~~F1: F1 change (in Hz)~~
2. ~~F2: F1 amplitude~~
3. ~~F3: F2 change (in Hz)~~
4. ~~F4: F2 amplitude~~
5. ~~F5: F3 change (in Hz)~~
6. ~~F6: F3 amplitude~~



This gives us 9 dimensions

### Stimuli

List 35, first 3 sentences

1. Most of the new is easy for us to hear. (1)
2. He used the lathe to make brass objects. (2)
3. The vane on top of the pole revolved in the wind. (3)



### Paradigm

Each participant completes two chains. Each participant only has one emotion (participant ID % 3, 0 = happy, 1 = sad, 2 = angry). Each chain has one sentence. Each chain has three sweeps, so each dimension is visited three times. This makes 27 iterations per chain and 54 iterations in total for one participant.



### Experiment structure

- Questionnaires
- Instructions
- Introduction of context
- Practice trial (two salient dimensions, e.g. pitch shift and jitter)
- Two chain blocks



## Analysis



# Open questions

- Gibbs slider resolution: many synthesis steps --> long stimulus
- Which questions do we want to ask

