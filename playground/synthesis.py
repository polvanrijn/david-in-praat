import os
from parselmouth.praat import call
import numpy as np
from scipy import interpolate

# Synthesis methods
###############
# PITCH
###############


def pitch_shift(pitch_values, reference_tone, shift_st):
    shift_hz = cent2herz(shift_st * 100, reference_tone) - reference_tone
    return pitch_values + shift_hz


def scale_pitch(pitch_values, scalar):
    # Get the current pitch range
    full_range = pitch_values.max() - pitch_values.min()
    half_range = full_range / 2

    # Center all pitch values around 0
    pitch_rel = pitch_values - pitch_values.min() - half_range

    # Multiply with scalar and put it back to the original pitch height
    return (pitch_rel * scalar) + pitch_values.min() + half_range


def inflection(time, pitch_values, points, reference_tone, start_time, duration_ms):
    # Convert all sound measures to seconds
    duration_s = duration_ms / 1000
    if not all([p >= 0 and p <= duration_ms for p in points[0]]):
        raise ValueError('Time must lay in specified duration')
    time_kernel = [start_time + p / 1000 for p in points[0]]

    # Convert cents to hertz
    pitch_kernel = [p for p in points[1]]

    idxs = [i for i, t in enumerate(time) if t >= start_time and t < start_time + duration_s]
    time = time[idxs]
    pitch_values = pitch_values[idxs]

    tck = interpolate.splrep(time_kernel, pitch_kernel, s=0)
    pitch_kernel_spline = interpolate.splev(time, tck, der=0)
    new_pitch_values = pitch_values + pitch_kernel_spline

    # import matplotlib.pyplot as plt
    # plt.figure()
    # plt.plot(time_kernel, pitch_kernel, 'g', time, pitch_values, 'r', time, pitch_kernel_spline, 'y', time, new_pitch_values)
    # plt.legend(['Original contour', 'Linear points', 'Spline interpolation', 'New contour'])
    # plt.show()

    return pitch_values + pitch_kernel_spline


def jitter(manipulation, pulses, jitter_amount):
    matrix = call(pulses, "To Matrix")
    r = jitter_amount / 100000

    formula = "self + randomGauss(0, %f)" % r
    call([matrix], "Formula", formula)

    pointprocess2 = call(matrix, "To PointProcess")
    call([pointprocess2, manipulation], "Replace pulses")
    return manipulation, pointprocess2


def update_pitch_points(pitch, manipulation, pitch_values, time):
    pitch_tier = call(manipulation, "Extract pitch tier")
    # Make sure the pitch Tier is empty
    call(pitch_tier, "Remove points between", min(pitch.xs()) - 0.001, max(pitch.xs()) + 0.001)
    for i in range(len(pitch_values)):
        call(pitch_tier, "Add point", time[i], pitch_values[i])
    call([manipulation, pitch_tier], "Replace pitch tier")
    return manipulation


###############
# DURATION
###############


def scale_duration(manipulation, duration, scalar):
    duration_tier = call("Create DurationTier", "tmp", 0, duration)
    call([duration_tier], "Add point", 0, scalar)
    call([duration_tier, manipulation], "Replace duration tier")
    return manipulation


###############
# INTENSITY
###############


def tremolo(duration, tremolo_rate, tremolo_depth):
    pulses = np.pi * (tremolo_rate * 2) / 100
    intensity_tier = call("Create IntensityTier", "tremolo", 0, duration)
    tim = round(duration * 100)
    ramp = 0

    for i in range(tim - 1):
        if i <= tim - 25 and ramp <= 1:
            ramp = ramp + 0.04
            if ramp > 1:
                ramp = 1
        else:
            ramp = ramp - 0.04
            if ramp < 0:
                ramp = 0
        intensity = 90 + (tremolo_depth / 2) * np.sin(pulses * i) * ramp
        call([intensity_tier], "Add point", i / 100, intensity)

    return intensity_tier


# def brightness(sound, new_F2, new_F3, maximum_formant):
#     praat_script = '/Users/pol.van-rijn/Library/Preferences/Praat Prefs/plugin_VocalToolkit/changeformants.praat'
#     return run_file(sound, praat_script, 0, new_F2, new_F3, 0, 0, maximum_formant, 1, 1)[0]


# Helper methods
def load_files(path):
    from parselmouth import Sound
    import json

    filename = os.path.basename(path)
    folder = path[:-len(filename)]

    # Use splitext() to get filename and extension separately.
    (file, ext) = os.path.splitext(filename)
    tg_path = folder + file + '.TextGrid'

    setting_path = folder + file + '.json'

    with open(setting_path) as settings_file:
        data = json.load(settings_file)

    data['base_name'] = file

    sound = Sound(path)
    data['duration'] = sound.xmax - sound.xmin
    data['step_size'] = 0.01
    pitch = call(sound, "To Pitch", data['step_size'], data['min_F0'], data['max_F0'])

    manipulation = call([sound, pitch], "To Manipulation")

    pitch_values = pitch.selected_array['frequency']

    # Remove NAs
    idxs = np.where(pitch_values == 0)
    pitch_values = np.delete(pitch_values, idxs)
    time = np.delete(pitch.xs(), idxs)

    pulses = call(pitch, "To PointProcess")

    grid = read_textgrid(tg_path)

    if 'sentence' in grid:
        data['sentence_durations'] = [
            {'start': i.xmin, 'end': i.xmax, 'label': i.text}
            for i in grid['sentence'] if i.text != ""
        ]

    else:
        raise Exception('TextGrid must contain interval tier `sentence`')
    return sound, manipulation, pitch, time, pitch_values, pulses, data


def read_textgrid(tg_path):
    import textgrids
    if os.path.exists(tg_path):
        return textgrids.TextGrid(tg_path)
    else:
        raise Exception('TextGrid must be present')


def cent2herz(ct, reference_tone):
    """Converts deviation in cents to a value in Hertz"""
    st = ct / 100
    semi1 = np.log(np.power(2, 1 / 12))
    return np.exp(st * semi1) * reference_tone
