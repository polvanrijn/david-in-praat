# Note: parselmouth must be installed with pip install praat-parselmouth
# Note: synth_stimulus is the only function required by the Audio Gibbs Sampler;

import os
from parselmouth.praat import call, run_file
import numpy as np
from playground.synthesis import (
    load_files,

    pitch_shift,
    update_pitch_points,
    jitter,
    inflection,
    scale_pitch,
    scale_duration,
    tremolo,
    cent2herz
)
from datetime import datetime, timedelta
from experiment.within.settings import SETTINGS

def make_stimuli(vector, output_path, chain_definition):
    """
    Synthesises a stimulus.

    Parameters
    ----------

    vector : list
        A vector of parameters as produced by the Gibbs sampler

    output_path : str
        The output path for the generated file.

    chain_definition
        The chain's definition object.
    """
    all_times = {}
    start = datetime.now()

    assert isinstance(chain_definition, dict)
    assert len(vector) == SETTINGS['DIMENSIONS']

    parameters = dict(zip(SETTINGS['DIMENSION_NAMES'], vector))

    # TODO CHANGED
    SYNTHESIS_FILES_DIR = 'sounds/harvard/'

    filename = os.path.join(SYNTHESIS_FILES_DIR, chain_definition['file'])
    sound, manipulation, pitch, time, pitch_values, pulses, data = load_files(filename)

    all_times['setup'] = datetime.now() - start
    start = datetime.now()
    if 'pitch_shift' in parameters and parameters['pitch_shift'] != 0:
        shift_st = int(parameters['pitch_shift'])
        pitch_values = pitch_shift(pitch_values, data['reference_tone'], shift_st)

    all_times['pitch_shift'] = datetime.now() - start
    start = datetime.now()

    if 'pitch_change' in parameters and parameters['pitch_change'] != 0:
        pitch_change = float(parameters['pitch_change'])
        pitch_change = cent2herz(pitch_change * 100, data['reference_tone'])
        for sentence in data['sentence_durations']:
            # Must be in ms
            duration_s = (sentence['end'] - sentence['start'])
            duration_ms = duration_s * 1000
            points = np.stack((
                np.linspace(0, duration_ms, num=4),
                np.linspace(0, pitch_change * duration_s, num=4)
            ))
            pitch_values = inflection(
                time, pitch_values, points, data['reference_tone'], sentence['start'], duration_ms
            )

    all_times['pitch_change'] = datetime.now() - start
    start = datetime.now()

    if 'pitch_range' in parameters and parameters['pitch_range'] != 1:
        scalar = float(parameters['pitch_range'])
        pitch_values = scale_pitch(pitch_values, scalar)

    all_times['pitch_range'] = datetime.now() - start
    start = datetime.now()

    manipulation = update_pitch_points(pitch, manipulation, pitch_values, time)

    all_times['update_pitch_points'] = datetime.now() - start
    start = datetime.now()

    if 'jitter' in parameters and parameters['jitter'] != 0:
        jitter_amount = (int(parameters['jitter']))
        manipulation, pulses = jitter(manipulation, pulses, jitter_amount)

    all_times['jitter'] = datetime.now() - start
    start = datetime.now()

    if 'duration' in parameters and parameters['duration'] != 1:
        duration_scalar = (float(parameters['duration']))
        manipulation = scale_duration(manipulation, data['duration'], duration_scalar)

    all_times['duration'] = datetime.now() - start
    start = datetime.now()

    sound = call(manipulation, "Get resynthesis (overlap-add)")

    all_times['synthesis'] = datetime.now() - start
    start = datetime.now()

    if all([name in parameters and parameters[name] != 0 for name in ['tremolo_depth', 'tremolo_rate']]):
        tremolo_depth = (float(parameters['tremolo_depth']))
        tremolo_rate = (float(parameters['tremolo_rate']))
        intensity_tier = tremolo(data['duration'], tremolo_rate, tremolo_depth)
        sound = call([sound, intensity_tier], "Multiply", "yes")

    all_times['tremolo'] = datetime.now() - start
    start = datetime.now()

    call(sound, "Save as WAV file", output_path)
    all_times['save_wav'] = datetime.now() - start
    return all_times

from random import uniform, sample
# times = []
# sum_time = timedelta(0, 0, 0)
# for i in range(100):
#     vector = []
#     for r in SETTINGS['RANGES']:
#         vector.append(uniform(r[0], r[1]))
#     chain_definition = {
#         'file': sample(SETTINGS['SENTENCE_RECORDINGS'], 1)[0]
#     }
#     output_path = '~/Desktop/test_%d.wav' % i
#     parameters = dict(zip(SETTINGS['DIMENSION_NAMES'], vector))
#     print(parameters)
#     all_times = make_stimuli(vector, output_path, chain_definition)
#     times.append(all_times)
#     total_time = timedelta(0, 0, 0)
#     for label, time in all_times.items():
#         total_time += time
#     sum_time += total_time
#     print("Full duration: %s" % total_time)
#
# print(sum_time)
#
chain_definition = {
        'file': sample(SETTINGS['SENTENCE_RECORDINGS'], 1)[0]
    }
vector = [
        # DURATION
        # 1. Duration, percent
        1,

        # INTENSITY
        # 2. Tremolo rate, st
        0,

        # 3. Tremolo depth, dB
        0,

        # PITCH
        # 4. Shift, semitones
        -3,

        # 5. Range, percent
        2,

        # 6. Increase/Decrease, semitones
        0,

        # 7. Jitter, custom unit
        0
    ]
output_path = '~/Desktop/test_shift.wav'
make_stimuli(vector, output_path, chain_definition)





# for key in all_times.keys():
#     time_arr = []
#     total_time = timedelta(0, 0, 0)
#     for t in times:
#         total_time += t[key]
#         time_arr.append(t[key])
#     print("%s:\t%s" % (key, total_time/len(time_arr)))

