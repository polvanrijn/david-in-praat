f1_diff = %d
f2_diff = %d
f3_diff = %d
maximum_formant=%d
s = selected("Sound")
s$ = selected$("Sound")
original_dur = Get total duration
int = Get intensity (dB)

if int <> undefined
	#runScript: "workpre.praat"
	wrk = selected("Sound")
	sf1 = Get sampling frequency

	selectObject: wrk
	hf = Filter (pass Hann band): maximum_formant, 0, 100

	selectObject: wrk
	sf2 = maximum_formant * 2
	rs1 = Resample: sf2, 10

	formant2 = noprogress nowarn To Formant (robust): 0.005, 5, maximum_formant, 0.025, 50, 1.5, 5, 0.000001
	old_formants = formant2

	lpc1 = noprogress To LPC: sf2
	plusObject: rs1
	source = Filter (inverse)

	selectObject: formant2
	filtr = Copy: "filtr"

    if f1_diff <> 0
	    Formula (frequencies): "if row = 1 then self + f1_diff else self fi"
	endif
	if f2_diff <> 0
	    Formula (frequencies): "if row = 1 then self + f2_diff else self fi"
	endif
	if f3_diff <> 0
	    Formula (frequencies): "if row = 1 then self + f3_diff else self fi"
	endif

	new_formants = formant2

	lpc2 = noprogress To LPC: sf2
	plusObject: source
	tmp = Filter: "no"

	rs2 = Resample: sf1, 10
	Formula: "self + object[hf]"

	#runScript: "workpost.praat", original_dur
	stt = Get start time
	et = Get end time

	Extract part: stt + 0.025, et - 0.025, "rectangular", 1, "no"
	#runScript: "fixdc.praat"
	final_dur = Get total duration

	if final_dur <> original_dur
		tmp = selected("Sound")
		Extract part: 0, original_dur, "rectangular", 1, "no"
		removeObject: tmp
	endif
	Scale intensity: int
	#runScript: "declip.praat"
	extr = Get absolute extremum: 0, 0, "None"

	if extr = undefined
		extr = 0
	endif

	if number(fixed$(extr, 2)) > 0.99
		Scale peak: 0.99
	endif


	dur = Get total duration
	if dur > 0.5
		Fade in: 0, 0, 0.005, "yes"
		Fade out: 0, dur, -0.005, "yes"
	endif

else
	Copy: "tmp"
endif