form
 	real db_upper 0
 	real cut_off_freq 5000
endform

procedure eqBand: .bnd1, .bnd2, .db, .smoothing
	.amp = 0.00002 * 10 ^ (.db / 20)
	selectObject: buffer
	Formula: "object[sp_pulse]"
	Filter (pass Hann band): .bnd1, .bnd2, .smoothing
	Formula: "self * .amp"
	selectObject: sp_eq
	Formula: "self + object[buffer]"
endproc

db_lower = 0

wrk = selected("Sound")
s$ = selected$("Sound")
sf = Get sampling frequency
dur1 = Get total duration
pointprocess = Create empty PointProcess: "pulse", 0, 0.05
Add point: 0.025

pulse = noprogress To Sound (pulse train): sf, 1, 0.05, 2000

sp_pulse = noprogress To Spectrum: "no"

buffer = Copy: "buffer"
Formula: "0"

sp_eq = Copy: "sp_eq"
@eqBand: 0, cut_off_freq, db_lower, 100
@eqBand: cut_off_freq, 20000, db_upper, 100
pulse_eq_tmp = noprogress To Sound
dur2 = Get total duration

pulse_eq = Extract part: (dur2 - 0.05) / 2, dur2 - ((dur2 - 0.05) / 2), "Hanning", 1, "no"
Scale peak: 0.99
pulse_sf = Get sampling frequency
if pulse_sf <> sf
	Override sampling frequency: sf
endif

plusObject: wrk
tmp1 = Convolve: "sum", "zero"

tmp2 = Extract part: 0.025, dur1 + 0.025, "rectangular", 1, "no"
Rename: s$ + "-" + string$ (cut_off_freq) + "Hz-" + string$ (db_upper) + "dB"
removeObject: pointprocess, pulse, sp_pulse, buffer, sp_eq, pulse_eq_tmp, pulse_eq, tmp1