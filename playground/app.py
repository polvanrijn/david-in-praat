from flask import Flask, render_template, request, send_file
from playground.synthesis import (
    pitch_shift,
    inflection,
    scale_pitch,
    update_pitch_points,
    jitter,

    scale_duration,
    tremolo,
    cent2herz,
    load_files
)
from parselmouth.praat import call
import json
import io
import matplotlib.pyplot as plt
import numpy as np

app = Flask(__name__)

#####################
# TEMPORARY STORAGE
####################

class DataStore():
    # unmanipulated versions
    sound = None
    pitch = None
    manipulation = None
    pitch_tier = None
    sentence_durations = None
    duration = None
    pulses = None

    synth_sound = None

    reference_tone = None
    F2 = None
    F3 = None

    # F0 settings
    step_size = 0.01
    min_F0 = None
    max_F0 = None
    maximum_formant = None

    # Plots
    plt_spec = None
    plt_wave = None
    plt_pitch = None
    plt_int = None


temp_stor = DataStore()


@app.route('/')
def show_editor():
    return render_template('demo.html')


@app.route('/prep-plot', methods=['GET'])
def plot():
    temp_stor.plt_spec = plot_spectrogram(temp_stor.synth_sound)
    temp_stor.plt_wave = plot_waveform(temp_stor.synth_sound)
    temp_stor.plt_pitch = plot_pitch(temp_stor.synth_sound)
    temp_stor.plt_int = plot_intensity(temp_stor.synth_sound)
    return ''


# @app.route('/make-plot/<duration>', methods=['GET'])
# def plot(duration):
#     temp_stor.plt_spec = plot_spectrogram(temp_stor.synth_sound)
#     temp_stor.plt_wave = plot_waveform(temp_stor.synth_sound)
#     temp_stor.plt_pitch = plot_pitch(temp_stor.synth_sound)
#     temp_stor.plt_int = plot_intensity(temp_stor.synth_sound)
#     return ''


@app.route('/plot-intensity', methods=['GET'])
def show_plot_intensity():
    return temp_stor.plt_int

@app.route('/plot-pitch', methods=['GET'])
def show_plot_pitch():
    return temp_stor.plt_pitch


@app.route('/plot-waveform', methods=['GET'])
def show_plot_wave_form():
    return temp_stor.plt_wave


@app.route('/plot-spectrogram', methods=['GET'])
def show_plot_spectrogram():
    return temp_stor.plt_spec


def get_image():
    bytes_image = io.BytesIO()
    plt.savefig(bytes_image, format='png')
    bytes_image.seek(0)
    return send_file(bytes_image,
                     attachment_filename='plot.png',
                     mimetype='image/png')


def plot_waveform(sound):
    plt.plot(sound.xs(), sound.values.T)
    plt.xlim([sound.xmin, sound.xmax])
    plt.xlabel("time [s]")
    plt.ylabel("amplitude")
    image = get_image()
    plt.clf()
    return image


def plot_intensity(sound):
    intensity = sound.to_intensity()
    plt.plot(intensity.xs(), intensity.values.T, linewidth=3, color='w')
    plt.plot(intensity.xs(), intensity.values.T, linewidth=1, color='r')
    plt.grid(False)
    plt.ylim(0)
    plt.ylabel("intensity [dB]")
    image = get_image()
    plt.clf()
    return image


def plot_pitch(sound):
    pitch = sound.to_pitch()
    pitch_values = pitch.selected_array['frequency']
    pitch_values[pitch_values == 0] = np.nan
    plt.plot(pitch.xs(), pitch_values, 'o', markersize=5, color='w')
    plt.plot(pitch.xs(), pitch_values, 'o', markersize=2)
    plt.grid(False)
    plt.ylim(0, pitch.ceiling)
    plt.ylabel("fundamental frequency [Hz]")
    plt.xlim([sound.xmin, sound.xmax])
    plt.savefig('pitch.pdf')
    image = get_image()
    plt.clf()
    return image


def plot_spectrogram(sound, dynamic_range=70):
    pre_emphasized_snd = sound.copy()
    pre_emphasized_snd.pre_emphasize()
    spectrogram = pre_emphasized_snd.to_spectrogram(window_length=0.03, maximum_frequency=8000)
    X, Y = spectrogram.x_grid(), spectrogram.y_grid()
    sg_db = 10 * np.log10(spectrogram.values)
    plt.pcolormesh(X, Y, sg_db, vmin=sg_db.max() - dynamic_range)
    plt.ylim([spectrogram.ymin, spectrogram.ymax])
    plt.xlabel("time [s]")
    plt.ylabel("frequency [Hz]")
    image = get_image()
    plt.clf()
    return image



@app.route('/synthesize', methods=['GET', 'POST'])
def synthesize():
    # Load all files
    filename = request.form['demo_sound']
    sound, manipulation, pitch, time, pitch_values, pulses, data = load_files(filename)

    if 'pitch_shift' in request.form:
        shift_st = int(request.form['pitch_shift'])
        pitch_values = pitch_shift(pitch_values, data['reference_tone'], shift_st)

    if 'pitch_range' in request.form:
        scalar = float(request.form['pitch_range'])
        pitch_values = scale_pitch(pitch_values, scalar)


    if 'pitch_slope' in request.form and request.form['pitch_slope'] != '0':
        pitch_change = float(request.form['pitch_slope'])
        pitch_change = cent2herz(pitch_change * 100, data['reference_tone']) - data['reference_tone']
        for sentence in data['sentence_durations']:
            # Must be in ms
            duration_s = (sentence['end'] - sentence['start'])
            duration_ms = duration_s * 1000
            points = np.stack((
                np.linspace(0, duration_ms, num=4),
                np.linspace(0, pitch_change * duration_s, num=4)
            ))
            pitch_values = inflection(
                time, pitch_values, points, data['reference_tone'], sentence['start'], duration_ms
            )

    manipulation = update_pitch_points(pitch, manipulation, pitch_values, time)

    if 'jitter' in request.form:
        jitter_amount = (int(request.form['jitter']))
        manipulation, pulses = jitter(manipulation, pulses, jitter_amount)

    if 'duration' in request.form:
        duration_scalar = (float(request.form['duration']))
        manipulation = scale_duration(manipulation, data['duration'], duration_scalar)

    sound = call(manipulation, "Get resynthesis (overlap-add)")

    if 'tremolo_depth' in request.form and 'tremolo_rate' in request.form:
        tremolo_depth = (float(request.form['tremolo_depth']))
        tremolo_rate = (float(request.form['tremolo_rate']))
        intensity_tier = tremolo(data['duration'], tremolo_rate, tremolo_depth)
        sound = call([sound, intensity_tier], "Multiply", "yes")

    pointProcess = call(sound, "To PointProcess (periodic, cc)", data['min_F0'], data['max_F0'])
    localJitter = call(pointProcess, "Get jitter (local)", 0, 0, 0.0001, 0.02, 1.3)
    localShimmer = call([sound, pointProcess], "Get shimmer (local)", 0, 0, 0.0001, 0.02, 1.3, 1.6)
    print('Shimmer: %f, Jitter: %f' % (localShimmer, localJitter))
    print(request.form)
    temp_stor.synth_sound = sound

    values = sound.values.T.tolist()

    return json.dumps({
        'values': values,
        'sample_rate': sound.sampling_frequency,
        'duration': len(values) / sound.sampling_frequency
    })
