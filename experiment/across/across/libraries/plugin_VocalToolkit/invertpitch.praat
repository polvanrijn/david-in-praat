include batch.praat

procedure action
	s$ = selected$("Sound")
	runScript: "changepitchmedian.praat", 0, -100
	Rename: s$ + "-invertpitch"
endproc
