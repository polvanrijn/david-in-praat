from settings import SETTINGS
from random import sample
from psynet.trial.audio_gibbs import (
    AudioGibbsNetwork, AudioGibbsTrial, AudioGibbsNode, AudioGibbsSource, AudioGibbsTrialMaker
)
from psynet.timeline import (
    join
)
from copy import copy
from flask import Markup
from timeline_parts import (
    make_instructions,
    make_general_instructions,
    make_message_after_practice,
)
import random

SYNTH_FUNCTION_LOCATION = {
    "module_name": "synthesis",
    "function_name": "make_stimuli"
}

S3_BUCKET = "audio-gibbs-demo"

def make_network_definition(self, target):
    all_recordings = copy(SETTINGS['SENTENCE_RECORDINGS'])

    # Randomly pick one
    file = sample(all_recordings, 1)[0]

    return {
        "target": target,
        "file": file
    }


class AngerNetwork(AudioGibbsNetwork):
    __mapper_args__ = {"polymorphic_identity": "anger_network"}

    synth_function_location = SYNTH_FUNCTION_LOCATION

    s3_bucket = S3_BUCKET
    vector_length = SETTINGS['DIMENSIONS']
    vector_ranges = SETTINGS['RANGES']
    granularity = SETTINGS['NUMBER_OF_SLIDER_TICKS']

    def make_definition(self):
        return make_network_definition(self, 'angry')


class HappinessNetwork(AudioGibbsNetwork):
    __mapper_args__ = {"polymorphic_identity": "happiness_network"}

    synth_function_location = SYNTH_FUNCTION_LOCATION

    s3_bucket = S3_BUCKET
    vector_length = SETTINGS['DIMENSIONS']
    vector_ranges = SETTINGS['RANGES']
    granularity = SETTINGS['NUMBER_OF_SLIDER_TICKS']

    def make_definition(self):
        return make_network_definition(self, 'happy')


class SadnessNetwork(AudioGibbsNetwork):
    __mapper_args__ = {"polymorphic_identity": "sadness_network"}

    synth_function_location = SYNTH_FUNCTION_LOCATION

    s3_bucket = S3_BUCKET
    vector_length = SETTINGS['DIMENSIONS']
    vector_ranges = SETTINGS['RANGES']
    granularity = SETTINGS['NUMBER_OF_SLIDER_TICKS']

    def make_definition(self):
        return make_network_definition(self, 'sad')


def get_trial_prompt(emotion):
    return Markup(
        "Adjust the slider to make the speaker sound like she is "
        f"<strong>{emotion}</strong>."
    )


class AngerTrial(AudioGibbsTrial):
    __mapper_args__ = {"polymorphic_identity": "anger_trial"}

    debug = SETTINGS['DEBUG']
    snap_slider = SETTINGS['SNAP_SLIDER']
    autoplay = SETTINGS['AUTOPLAY']
    minimal_time = SETTINGS['MIN_DURATION']

    def choose_reverse_scale(self):
        return SETTINGS['REVERSE_SCALE']

    def get_prompt(self, experiment, participant):
        return get_trial_prompt(self.network.definition['target'])


class HappinessTrial(AudioGibbsTrial):
    __mapper_args__ = {"polymorphic_identity": "happiness_trial"}

    debug = SETTINGS['DEBUG']
    snap_slider = SETTINGS['SNAP_SLIDER']
    autoplay = SETTINGS['AUTOPLAY']
    minimal_time = SETTINGS['MIN_DURATION']

    def choose_reverse_scale(self):
        return SETTINGS['REVERSE_SCALE']

    def get_prompt(self, experiment, participant):
        return get_trial_prompt(self.network.definition['target'])


class SadnessTrial(AudioGibbsTrial):
    __mapper_args__ = {"polymorphic_identity": "sadness_trial"}

    debug = SETTINGS['DEBUG']
    snap_slider = SETTINGS['SNAP_SLIDER']
    autoplay = SETTINGS['AUTOPLAY']
    minimal_time = SETTINGS['MIN_DURATION']

    def choose_reverse_scale(self):
        return SETTINGS['REVERSE_SCALE']

    def get_prompt(self, experiment, participant):
        return get_trial_prompt(self.network.definition['target'])


class CustomNode(AudioGibbsNode):
    __mapper_args__ = {"polymorphic_identity": "custom_node"}


class CustomSource(AudioGibbsSource):
    __mapper_args__ = {"polymorphic_identity": "custom_source"}

    def generate_seed(self, network, experiment, participant):
        if network.vector_length is None:
            raise ValueError("network.vector_length must not be None. Did you forget to set it?")
        return {
            "vector": SETTINGS['INITIAL_VALUES'],
            "active_index": random.randint(0, network.vector_length),
        }

class CustomTrialMaker(AudioGibbsTrialMaker):
    response_timeout_sec = 1e9

def make_block(target, phase="experiment"):
    if target == 'angry':
        network_class = AngerNetwork
        trial_class = AngerTrial
    elif target == 'happy':
        network_class = HappinessNetwork
        trial_class = HappinessTrial
    elif target == 'sad':
        network_class = SadnessNetwork
        trial_class = SadnessTrial
    else:
        raise NotImplementedError()

    if phase == 'experiment':
        return CustomTrialMaker(
            network_class=network_class,
            trial_class=trial_class,
            node_class=CustomNode,
            source_class=CustomSource,
            phase=phase,  # can be whatever you like
            time_estimate_per_trial=SETTINGS['TIME_ESTIMATE_PER_TRIAL'],
            chain_type="across",  # changed, can be "within" or "across"
            num_trials_per_participant=SETTINGS['NUM_TRIALS_PER_PARTICIPANT'],  # i.e. 42
            num_nodes_per_chain=SETTINGS['ITERATIONS_PER_CHAIN'],  # i.e. 21
            num_chains_per_participant=None,  # changed, set to None if chain_type="across"
            num_chains_per_experiment=SETTINGS['NUM_CHAINS_PER_EXPERIMENT'],  # changed, set to None if chain_type="within"
            trials_per_node=1,
            active_balancing_across_chains=True,
            check_performance_at_end=False,
            check_performance_every_trial=False,
            propagate_failure=False,
            recruit_mode="num_trials",  # changed
            target_num_participants=None  # changed
        )
    else:
        return CustomTrialMaker(
            network_class=network_class,
            trial_class=trial_class,
            node_class=CustomNode,
            source_class=CustomSource,
            phase=phase,  # can be whatever you like
            time_estimate_per_trial=SETTINGS['TIME_ESTIMATE_PER_TRIAL'],
            chain_type="within",  # can be "within" or "across"
            num_trials_per_participant=2,
            num_nodes_per_chain=2,
            num_chains_per_participant=1,  # set to None if chain_type="across"
            num_chains_per_experiment=None,  # set to None if chain_type="within"
            trials_per_node=1,
            active_balancing_across_chains=True,
            check_performance_at_end=False,
            check_performance_every_trial=False,
            propagate_failure=False,
            recruit_mode="num_participants",
            target_num_participants=0
        )

def make_experiment(target):
    return join(
        # Make general instructions
        make_general_instructions(target),

        # Make instructions
        make_instructions(target, initial=True),

        # Add some practice trials
        make_block(target, phase="training"),
        make_message_after_practice(),

        # The main experiment
        make_block(target)
    )
