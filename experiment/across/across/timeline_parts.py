from psynet.headphone import headphone_check
from psynet.timeline import join
from psynet.page import (
    Page,
    InfoPage,
    NumberInputPage,
    NAFCPage,
    TextInputPage,
    VolumeCalibration,
)
from flask import Markup
import os
import json


def get_template(name):
    assert isinstance(name, str)
    data_path = os.path.join('templates', name)
    with open(data_path, encoding='utf-8') as fp:
        template_str = fp.read()
    return template_str


class LanguagePage(Page):
    """
        This page solicits a text response from the user.
        By default this response is saved in the database as a
        :class:`psynet.timeline.Response` object,
        which can be found in the ``Questions`` table.

        Parameters
        ----------

        label:
            Internal label for the page (used to store results).

        prompt:
            Prompt to display to the user. Use :class:`flask.Markup`
            to display raw HTML.

        time_estimate:
            Time estimated for the page.

        **kwargs:
            Further arguments to pass to :class:`psynet.timeline.Page`.
        """

    def __init__(
            self,
            label: str,
            prompt: str,
            time_estimate: float,
            **kwargs
    ):
        self.prompt = prompt
        with open('languages.json', 'r') as f:
            languages = json.load(f)
        super().__init__(
            time_estimate=time_estimate,
            template_str=get_template("language-input-page.html"),
            label=label,
            template_arg={
                "prompt": prompt,
                "languages": languages
            },
            **kwargs
        )

    def metadata(self, **kwargs):
        # pylint: disable=unused-argument
        return {
            "prompt": self.prompt
        }


make_feedback_form = join(TextInputPage(
    "feedback",
    Markup("""
                 Did you like the experiment?
                 """),
    time_estimate=5,
    one_line=False
),
    TextInputPage(
        "technical_problems",
        Markup("""
                 Did you encounter any technical problems during the
                 experiment? <br>If so, please provide a few words describing the
                 problem.
                 """),
        time_estimate=5,
        one_line=False
    ))

make_questionnaire = join(
    # Demographic data
    NumberInputPage(
        label='age',
        prompt='What is your age?',
        time_estimate=20
    ),
    NAFCPage(
        label='gender',
        prompt='With what gender do you most identify yourself?',
        time_estimate=20,
        choices=['male', 'female', 'other']
    ),
    NAFCPage(
        label='education',
        prompt='What is your highest educational qualification?',
        time_estimate=20,
        choices=['none', 'elementary school', 'middle school', 'high school', 'bachelor', 'master', 'PhD']
    ),
    LanguagePage(
        'daily_language',
        'Which language(s) do you most frequently speak in your daily life?',
        time_estimate=30
    ),
    LanguagePage(
        'child_language',
        'Which language(s) did you speak during childhood?',
        time_estimate=30
    ),
)

make_sound_check = join(
    # Perform volume check
    VolumeCalibration(),
    headphone_check(),
)


def make_instructions(target, initial=False):
    with open("instructions/%s.html" % target, "r") as f:
        text = f.read()
    context = InfoPage(Markup(text), time_estimate=60)
    if initial:
        return InfoPage(Markup(text + "<br><br> We will start with an example."), time_estimate=3)
    else:
        return context

def make_general_instructions(target):
    with open("instructions/instructions.html", "r") as f:
        instruction_text = f.read()

    return InfoPage(Markup(instruction_text % target), time_estimate=60)

def make_message_after_practice():
    return InfoPage(Markup('You will now start the experiment.'), time_estimate=10)