# pylint: disable=unused-import,abstract-method,unused-argument,no-member

##########################################################################################
# Settings
##########################################################################################
# Please review the in settings.py
from settings import SETTINGS
##########################################################################################
# Imports
##########################################################################################

import psynet.experiment
from psynet.timeline import (
    Timeline,
    CodeBlock,
    switch
)
from psynet.page import SuccessfulEndPage
from network_and_trial_definitions import make_experiment

from timeline_parts import (
    make_feedback_form,
    make_sound_check,
    make_questionnaire
)
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)


##########################################################################################
# Experiment
##########################################################################################

class Exp(psynet.experiment.Experiment):
    timeline = Timeline(
        # TODO UNCOMMENT BEFORE SANDBOXING
        #make_sound_check,
        #make_questionnaire,

        # Main experiment
        CodeBlock(
            lambda experiment, participant: participant.var.set(
                "emotion", SETTINGS['TARGETS'][participant.id % len(SETTINGS['TARGETS'])]
            )
        ),

        switch(
            "main_block",
            lambda experiment, participant: participant.var.emotion,
            {
                'angry': make_experiment('angry'),
                'sad': make_experiment('sad'),
                'happy': make_experiment('happy'),
            },
            fix_time_credit=False
        ),

        # Ask for some feedback
        make_feedback_form,
        SuccessfulEndPage()
    )


    def __init__(self, session=None):
        super().__init__(session)
        # Change this if you want to simulate multiple simultaneous participants.
        self.initial_recruitment_size = SETTINGS['INITIAL_RECRUITMENT_SIZE']


extra_routes = Exp().extra_routes()
