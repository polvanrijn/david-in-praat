# pylint: disable=unused-import,abstract-method

##########################################################################################
#### Imports
##########################################################################################

from flask import Markup
from psynet.headphone import headphone_check
from psynet.page import (
    VolumeCalibration,
)
import psynet.experiment
from psynet.timeline import (
    CodeBlock,
    Timeline
)

import json

from typing import (
    Union,
    Optional
)

from psynet.modular_page import (
    ModularPage,
    AudioPrompt,
    NAFCControl,
)

from psynet.page import (
    InfoPage,
    SuccessfulEndPage,
    TextInputPage
)
from psynet.trial.non_adaptive import (
    NonAdaptiveTrialMaker,
    NonAdaptiveTrial,
    StimulusSet,
    StimulusSpec,
)

import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

INITIAL_RECRUITMENT = 11

TRIALS_PER_PARTICIPANT_PER_BLOCK = 49
OPTIONS = [
    "1. Not at all ",
    "2. A little",
    " 3. Quite a lot",
    " 4. Very much "
]
NUM_LISTS = 109  # 109 different stimulus sets
BUCKET_URL = 'https://emo-validation.s3.amazonaws.com/'
TIME_PER_TRIAL = 5
TARGET_PARTICIPANTS = 150

##########################################################################################
#### Stimuli
##########################################################################################

stimulus_set = StimulusSet([
    StimulusSpec(
        definition={"stimulus_number": stimulus_number},
        phase="experiment",
        block=block
    )
    for stimulus_number in range(TRIALS_PER_PARTICIPANT_PER_BLOCK)
    for block in ['angry', 'happy', 'sad']
])

def make_general_instructions():
    with open("instructions/instructions.html", "r") as f:
        instruction_text = f.read()
    return InfoPage(Markup(instruction_text), time_estimate=60)

def make_instructions(target):
    with open("instructions/%s.html" % target, "r") as f:
        text = f.read()
    return Markup(text)


class CustomAudioPrompt(AudioPrompt):
    external_template = 'audio-prompt.html'

    def __init__(
            self,
            url: str,
            text: Union[str, Markup],
            loop: bool = False,
            prevent_response: bool = True,
            prevent_submit: bool = True,
            enable_submit_after: Optional[float] = None,
            start_delay=0.0,
            text_align="left",
            modal=''
    ):
        self.modal = modal

        super().__init__(
            url=url,
            text=text,
            loop=loop,
            prevent_response=prevent_response,
            prevent_submit=prevent_submit,
            enable_submit_after=enable_submit_after,
            start_delay=start_delay,
            text_align=text_align
        )


class ValidationTrial(NonAdaptiveTrial):
    __mapper_args__ = {"polymorphic_identity": "validation_trial"}

    def show_trial(self, experiment, participant):
        block = self.block
        last_block = participant.var.last_block
        stimulus_number = self.definition['stimulus_number']
        modal = ""
        if last_block != block:
            modal = make_instructions(block)
        filename = participant.var.stimuli[stimulus_number]
        page = ModularPage(
            label="response",
            prompt=CustomAudioPrompt(
                url=BUCKET_URL + filename,
                text=Markup(f"Does the speaker sound like she is <strong>{block}</strong>?"),
                modal=modal
            ),
            control=NAFCControl(OPTIONS),
            time_estimate=TIME_PER_TRIAL
        )

        participant.var.set('last_block', block)
        return page


##########################################################################################
#### Experiment
##########################################################################################

# Weird bug: if you instead import Experiment from psynet.experiment,
# Dallinger won't allow you to override the bonus method
# (or at least you can override it but it won't work).


def get_stimulus_set_by_participant_id(participant_id):
    # Participant IDs start at 1 and we start at 0
    list_num = (participant_id - 1) % NUM_LISTS
    with open('stimuli_lists/%d.json' % list_num) as json_file:
        data = json.load(json_file)
    return data


class Exp(psynet.experiment.Experiment):
    timeline = Timeline(
        # Perform volume check
        VolumeCalibration(),
        headphone_check(),
        make_general_instructions(),
        CodeBlock(lambda experiment, participant:
                  participant.var.set("stimuli", get_stimulus_set_by_participant_id(participant.id))),
        CodeBlock(lambda experiment, participant: participant.var.set("last_block", '')),
        NonAdaptiveTrialMaker(
            trial_class=ValidationTrial,
            phase="experiment",
            stimulus_set=stimulus_set,
            time_estimate_per_trial=TIME_PER_TRIAL,
            new_participant_group=True,
            max_trials_per_block=TRIALS_PER_PARTICIPANT_PER_BLOCK,
            allow_repeated_stimuli=False,
            max_unique_stimuli_per_block=None,
            active_balancing_within_participants=False,
            active_balancing_across_participants=False,
            check_performance_at_end=False,
            check_performance_every_trial=False,
            target_num_participants=TARGET_PARTICIPANTS,
            target_num_trials_per_stimulus=None,
            recruit_mode="num_participants",
            num_repeat_trials=0
        ),
        TextInputPage(
            "feedback",
            Markup("""
                     Did you like the experiment?
                     """),
            time_estimate=5,
            one_line=False
        ),
        TextInputPage(
            "technical_problems",
            Markup("""
                     Did you encounter any technical problems during the
                     experiment? <br>If so, please provide a few words describing the
                     problem.
                     """),
            time_estimate=5,
            one_line=False
        ),
        SuccessfulEndPage()
    )

    def __init__(self, session=None):
        super().__init__(session)
        self.initial_recruitment_size = INITIAL_RECRUITMENT


extra_routes = Exp().extra_routes()
