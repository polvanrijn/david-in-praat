import os
from zipfile import ZipFile
import shutil
import pandas as pd
import json
from copy import copy
import re

ITERATION0_BASENAME_PATTERN = re.compile("^iteration0_sentence[1-3].wav$")
AVG_ITERATION21_BASENAME_PATTERN = re.compile("^(HAP|SAD|ANG)_iteration21_sentence[1-3].wav$")
RANDOM_SAMPLE_BASENAME_PATTERN = re.compile("^random_list\d+_sentence[1-3].wav$")

def test_regex(pattern, string):
    return pattern.match(string) is not None

def extract_data(zip_path, csv_filename):
    if not os.path.exists(zip_path):
        raise ValueError('The path %s does not exist' % zip_path)

    # Create a ZipFile Object and load sample.zip in it
    with ZipFile(zip_path, 'r') as zipObj:
        # Extract all the contents of zip file in current directory
        zipObj.extractall('temp')

    question = pd.read_csv('temp/data/question.csv').sort_values(by='id')
    question = question[question.question == 'response']
    nrows, ncols = question.shape
    data = pd.DataFrame()

    for r in range(nrows):
        row = question.iloc[r,:]
        details = json.loads(row['details'])
        filename = details['prompt']['url'][40:]
        asked_emotion = details['prompt']['text'][43:46].upper()
        if test_regex(ITERATION0_BASENAME_PATTERN, filename):
            type = 'iteration0'
            iteration = 0
            sentence = filename[19]
            intended_emotion = None
            gibbs_participant = None
        elif test_regex(AVG_ITERATION21_BASENAME_PATTERN, filename):
            type = 'avg_iteration21'
            iteration = 21
            sentence = filename[24]
            intended_emotion = filename[:3]
            gibbs_participant = 'avg'
        elif test_regex(RANDOM_SAMPLE_BASENAME_PATTERN, filename):
            type = 'random'
            iteration = None
            sentence = filename[-5:-4]
            intended_emotion = None
            gibbs_participant = None
        else:
            type = 'raw_trial'
            spl_fn = filename.split('_')
            intended_emotion = spl_fn[0]
            gibbs_participant = spl_fn[1]
            iteration = spl_fn[2]
            sentence = spl_fn[3][0]
        data = data.append(pd.DataFrame({
            'rating': row['response'],
            'asked_emotion': asked_emotion,
            'participant': row['participant_id'],
            'filename': filename,
            'intended_emotion': intended_emotion,
            'gibbs_participant': gibbs_participant,
            'iteration': iteration,
            'sentence': sentence,
            'type': type
        }, index=[0]), ignore_index=True)

    data.to_csv(csv_filename)

    shutil.rmtree('temp', ignore_errors=True)

extract_data('data/within-validation2.zip', 'data.csv')