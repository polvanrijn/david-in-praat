import os
import pandas as pd
import json
from math import floor
from synthesis.synthesis import make_stimuli
from synthesis.settings import SETTINGS
from random import uniform, sample

BUCKET_NAME = 'emo-validation'
UPLOAD_TO_S3 = True
CREATE_STIMULI = True
STIMULI_FOLDER = '../validation_stimuli/'
SENTENCE_BASENAME = 'Harvard_L35_S0%d_0.wav'
ITERATION0_BASENAME = 'iteration0_sentence%d.wav'
AVG_ITERATION21_BASENAME = '%s_iteration21_sentence%d.wav'
RANDOM_SAMPLE_BASENAME = 'random_list%d_sentence%d.wav'
NUM_STIMULI_ALL_ITERATIONS = 42  # so each iteration two times
EMOTIONS = ['ANG', 'SAD', 'HAP']
SENTENCES = [1, 2, 3]
ITERATIONS = [i + 1 for i in list(range(21))]

if UPLOAD_TO_S3:
    # Make sure all stimuli are empty
    os.system('aws s3 rm s3://%s --recursive' % BUCKET_NAME)

if CREATE_STIMULI:
    # Create stimulus directories
    if not os.path.exists(STIMULI_FOLDER):
        os.mkdir(STIMULI_FOLDER)

    if not os.path.exists('stimuli_lists'):
        os.mkdir('stimuli_lists')

    # Read the correct experiment df
    df = pd.read_csv('../../data/gibbs_experiment/export_CORRECTED.csv')
    df['emotion'] = [emo.upper()[:3] for emo in df.emotion]

    # Should be 109
    num_lists = floor(df.shape[0] / NUM_STIMULI_ALL_ITERATIONS)

    chain_definition = {}

    # Shuffle the data frame
    df = df.sample(frac=1)

    df_random_samples = pd.DataFrame()

    def shuffle_from_available(visited_values):
        values = visited_values.values()
        keys = list(visited_values.keys())
        lowest = min(values)
        selected_key = sample([keys[idx] for idx, v in enumerate(values) if v == lowest], 1)[0]
        visited_values[selected_key] += 1
        return selected_key


    # Create iteration 0
    vector = SETTINGS['INITIAL_VALUES']
    for sentence in SENTENCES:
        chain_definition['file'] = SENTENCE_BASENAME % sentence
        output_path = STIMULI_FOLDER + ITERATION0_BASENAME % sentence
        make_stimuli(vector, output_path, chain_definition, SETTINGS)

    # Create average at iteration 21
    average_final_iteration = pd.read_csv('../../data/gibbs_experiment/average_at_final_iteration.csv')
    average_final_iteration['emotion'] = [emo.upper()[:3] for emo in average_final_iteration.emotion]

    for emo in EMOTIONS:
        row = average_final_iteration[average_final_iteration.emotion == emo]
        vector = row[SETTINGS['DIMENSION_NAMES']].values[0]
        for sentence in SENTENCES:
            chain_definition['file'] = SENTENCE_BASENAME % sentence
            output_path = STIMULI_FOLDER + AVG_ITERATION21_BASENAME % (emo, sentence)
            make_stimuli(vector, output_path, chain_definition, SETTINGS)


    for i in range(num_lists):
        files_in_split = []
        visited_emotions = dict(zip(EMOTIONS, [0] * len(EMOTIONS)))
        visited_iterations = dict(zip(ITERATIONS, [0] * len(ITERATIONS)))
        while len(files_in_split) < NUM_STIMULI_ALL_ITERATIONS:
            selected_emotion = shuffle_from_available(visited_emotions)
            selected_iteration = shuffle_from_available(visited_iterations)
            rows = df[(df.emotion == selected_emotion) & (df.iteration == selected_iteration)]
            if rows.shape[0] == 0:
                continue
            else:
                row = rows.iloc[0, :]

                # Append filename to list
                files_in_split.append(row['filename'])

                # do synthesis
                vector = row[SETTINGS['DIMENSION_NAMES']].values
                chain_definition['file'] = SENTENCE_BASENAME % row.sentence
                output_path = STIMULI_FOLDER + row.filename
                make_stimuli(vector, output_path, chain_definition, SETTINGS)

                # Remove from DF
                df = df.drop(row.name)

        # add 3 random samples
        for sentence in SENTENCES:
            vector = [uniform(r[0], r[1]) for r in SETTINGS['RANGES']]
            chain_definition['file'] = SENTENCE_BASENAME % sentence
            file_in_split = RANDOM_SAMPLE_BASENAME % (i, sentence)
            output_path = STIMULI_FOLDER + file_in_split
            make_stimuli(vector, output_path, chain_definition, SETTINGS)

            random_sample_row = dict(zip(SETTINGS['DIMENSION_NAMES'], vector))
            random_sample_row['filename'] = file_in_split
            df_random_samples = df_random_samples.append(random_sample_row, ignore_index=True)

            files_in_split.append(file_in_split)

        # add 1 iteration 0, randomly pick a sentence
        files_in_split.append(ITERATION0_BASENAME % sample(SENTENCES, 1)[0])

        # add average of last iteration for all 3 sentences
        random_sentences = sample(SENTENCES, 3)
        for s in range(len(random_sentences)):
            files_in_split.append(AVG_ITERATION21_BASENAME % (EMOTIONS[s], random_sentences[s]))

        with open('stimuli_lists/%d.json' % i, 'w') as outfile:
            json.dump(files_in_split, outfile)

        print('Finished %d' % i)

    df_random_samples.to_csv('stimuli_lists/random_samples_df.csv')

if UPLOAD_TO_S3:
    # move them to S3
    os.system('aws s3 mv %s s3://%s --recursive' % (STIMULI_FOLDER, BUCKET_NAME))
